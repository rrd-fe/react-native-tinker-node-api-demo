/**
 * Create by wangcheng on 10/25/18
 */

const config = require('config');

const MysqlFactory = require('../util/mysql_factory');

const APP_TABLE = 'tb_app';
const APP_FULL_PACKAGE_TABLE = 'tb_package';
const APP_PATCH_PACKAGE_TABLE = 'tb_patch';

const mysqlConfig = config.get('mysqlConfig');

const mysqlInstance = MysqlFactory.getInstance(mysqlConfig);

class RNPackage {

    /**
     * 根据appKey获取对应的app
     * @param appKey
     * @returns {Promise<*|null>}
     */
    static async getAppByKey(appKey){
        let sql = `select * from ${APP_TABLE} where appKey = ? limit 1`;
        const results = await mysqlInstance.query(sql, [ appKey ]);
        return results[0] || null;
    }

    /**
     * 获取对应appVersion的最新版全量包信息
     * @param appId
     * @param appVersion
     * @param rnVersion
     * @returns {Promise<*|null>}
     */
    static async getFullPackage(appId, appVersion, rnVersion){
        //status = 2 是对外开发的版本
        let sql = `select * from ${APP_FULL_PACKAGE_TABLE} where appId = ? and appVersion = ? and packageVersion > ? and status = 2 order by packageVersion DESC limit 1`;
        const results = await mysqlInstance.query(sql, [
            appId, appVersion, rnVersion
        ]);
        return results[0] || null;
    }

    /**
     * 获取对应rnVersion的最新版增量包信息
     * @param appId
     * @param fullPackageId
     * @param rnVersion
     * @returns {Promise<*|null>}
     */
    static async getPatchPackage(appId, fullPackageId, rnVersion){
        let sql = `select * from ${APP_PATCH_PACKAGE_TABLE} where appId = ? and packageId = ? and compareVersion = ?`;
        const results = await mysqlInstance.query(sql, [
           appId, fullPackageId, rnVersion
        ]);
        return results[0] || null;
    }

}

module.exports = RNPackage;