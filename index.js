/**
 * Create by wangcheng on 10/25/18
 */

const path = require('path');
const Koa = require('koa');
const KoaRouter = require('koa-router');
const koaStatic = require('koa-static');
const config = require('config');

const RNPackage = require('./model/RNPackage');

const serverConfig = config.get('serverConfig');
const packageConfig = config.get('packageConfig');

const app = new Koa();
const route = KoaRouter();
const dataRootDir = packageConfig.rootDir;

//全量代码包、增量代码包下载URL的前缀
let urlPrefix = '';

/**
 * 1. 利用appKey找到对应的app_id
 * 2. 利用app_id、clientVersion、rnVersion找到合适clientVersion的最新全量包
 * 3. 检查全量包ABTest是否匹配条件，是否能够对外开放
 * 4. 利用以上信息查找对应的增量包信息
 */
route.get('/rn/checkUpdate', async function(ctx, next){

    urlPrefix = `${ctx.request.protocol}://${ctx.request.host}/`;

    const request = ctx.request;
    const query = request.query;
    let { appKey, clientVersion, rnVersion } = query;
    let result = {
        status : 0,
        message : '',
        data : null
    };
    rnVersion = parseInt(rnVersion, 10);
    if(!appKey || !clientVersion || isNaN(rnVersion)){
        result = {
            status : 1,
            message : '[appKey|clientVersion|rnVersion]参数非法',
            data : null
        };
        ctx.body = result;
        return ;
    }

    //1. 利用appKey找到对应的app_id
    let app = null;
    try{
        app = await RNPackage.getAppByKey(appKey);
    }catch (error) {
        console.error(error.message);
        result = {
            status : 1,
            message : '获取App失败',
            data : null
        };
        ctx.body = result;
        return ;
    }
    if( !app ){
        result = {
            status : 1,
            message : 'App不存在！',
            data : null
        };
        ctx.body = result;
        return ;
    }

    //2. 利用 appId、clientVersion、rnVersion找到合适clientVersion的最新全量包
    let updateData = null;
    let fullPackage = null;
    try{
        fullPackage = await RNPackage.getFullPackage(app.id, clientVersion, rnVersion);
    }catch (error) {
        console.error(error.message);
        result = {
            status : 1,
            message : '获取App的全量包失败',
            data : null
        };
        ctx.body = result;
        return ;
    }
    if( !fullPackage ){
        updateData = {
            needUpdate : false
        };
        result = {
            status : 0,
            message : '未找到全量包！',
            data : updateData
        };
        ctx.body = result;
        return ;
    }

    //3. 检查全量包ABTest是否匹配条件，是否能够对外开放

    /**
     * abTest仅仅是发版时，填写的一项 可选 内容，具体填写什么格式，写什么内容，由最终开发者决定
     * demo这里，只展示根据ip来灰度发布的情况
     * 开发者可以有其他的回调条件，比如根据平台，根据用户ID等等
     */

    let isOpen = true;
    if( fullPackage.abTest ){
        //数据库里的 abTest 是个 string，需要解析成JSON
        let abTest = null;
        try{
            abTest = JSON.parse(fullPackage.abTest);
        }catch(err){
            abTest = null;
            console.error(`解析abTest配置失败: ${err.message}`);
        }

        if( abTest && Array.isArray(abTest.ips)){
            //假如 abTest 中配置了 IP 限制，检查当前用户的ip是否满足条件
            let clientIp = ctx.request.ip;
            isOpen = abTest.ips.includes(clientIp);
        }
    }
    if( !isOpen ){
        updateData = {
            needUpdate : false
        };
        result = {
            status : 0,
            message : '暂未开放下载！',
            data : updateData
        };
        ctx.body = result;
        return ;
    }

    updateData = {
        needUpdate : true,
        //是否强制更新标记
        forceUpdate: fullPackage.forceUpdate === 1,
        fullPackageMd5 : fullPackage.md5,
        fullPackageUrl : getPackageUrl(fullPackage.filePath),
        updateRNVersion : fullPackage.packageVersion,
    };

    //4. 利用以上信息查找对应的增量包信息
    let patchPackage = null;
    //disablePatch == 0 开发增量包下载
    if(fullPackage.disablePatch === 0){
        try {
            patchPackage = await RNPackage.getPatchPackage(app.id, fullPackage.id, rnVersion);
        }catch (error) {
            console.error(error.message);
        }
    }

    if(patchPackage){
        let patchData = {
            patchMd5 : patchPackage.md5,
            patchUrl : getPackageUrl(patchPackage.filePath),
        };
        Object.assign(updateData, patchData);
    }

    result = {
        status : 0,
        message : '成功！',
        data : updateData
    };
    ctx.body = result;
});


app.use(route.routes());
app.use(route.allowedMethods());

// 在本Demo中利用koaStatic提供代码zip包下载
// 生产环境中推荐大家使用Nginx提供静态资源的下载服务
app.use(koaStatic(dataRootDir, {
    hidden : true
}));

/**
 * 根据zip包路径返回下载URL，配合koaStatic或Nginx计算
 * @param filepath
 * @returns {string}
 */
function getPackageUrl(filepath){
    let relativePath = path.relative(dataRootDir, filepath);
    return `${urlPrefix}${relativePath}`;
}


app.on('error', (err, ctx) => {
    console.log('server error', err, ctx);
});

app.listen(serverConfig.port);