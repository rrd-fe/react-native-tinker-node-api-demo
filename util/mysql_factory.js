/**
 * Create by wangcheng on 10/25/18
 */

const mysql = require('mysql');

class MysqlFactory {

    /**
     * 获取操作数据库单例
     * @param mysqlConfig
     * @returns {MysqlFactory}
     */
    static getInstance(mysqlConfig){
        if(!this.mysqlInstance){
            this.mysqlInstance = new this(mysqlConfig);
        }
        return this.mysqlInstance;
    }

    constructor(mysqlConfig){
        this.pool = mysql.createPool(mysqlConfig);
    }

    /**
     * mysql query方法封装promise化
     * @param sql
     * @param values
     * @returns {Promise<any>}
     */
    query(sql, values){
        return new Promise( (resolve, reject)=> {
            this.pool.query(sql, values, function(error, results, fields){
                if(error){
                    reject(error);
                }else{
                    resolve(results);
                }
            });
        })
    }

}

module.exports = MysqlFactory;