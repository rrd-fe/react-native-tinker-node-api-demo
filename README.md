# react-native-tinker-node-api-demo

本工程配合react-native-tinker热更新服务使用，node.js版本。给App提供热更新查询和更新接口。

该工程仅作为参考Demo，生产环境可以根据现有工程情况选择node.js、Java、PHP等语言开发。

## 安装

### 方法一 ： Docker部署
docker pull rrdfe/leekserver:noahwdemo(2)    
docker pull dushaobin/leekserver:noahwdemo(2)       

注：docker image的名称待定，括号内为发布的版本这个在正式版的时候可能会有变化

启动docker

```
docker run -d \
    -p 3000:3000 \
    -p 9030:9030 \
    -p 3306:3306 \
    -v ~/packages:/usr/app/packages \
    dushaobin/leekserver
```

启动完成后可以通过 9030 端口来访问noah system服务    
使用 3000 端口来访问node demo

### 方法二 ：独立部署

* 下载仓库代码
* 修改config.js中的数据库、服务器、更新包根路径等相关配置
* yarn install 安装依赖包
* npm run 启动服务器

## 使用

* [API接口说明文档](./doc/api.md)