
## node-api-demo的接口说明文档

### 接口说明 **1、**检查更新

- **请求URL**
> [/rn/checkUpdate](#)

- **请求方式**
>**GET**

- **请求参数**
>
| 请求参数      |     参数类型 |   参数说明   |
| :-------- | :--------| :------ |
| appKey|  <mark>String,**必选**</mark>| 热更新APP的KEY  |
| clientVersion | <mark>String,**必选**</mark> | 客户端版本号 |
| rnVersion | <mark>Integer,**必选**</mark> | RN版本号 |

- **返回参数**
>
| 返回参数      |     参数类型 |   参数说明   |
| :-------- | :--------| :------ |
| status| Integer| 请求成功与否 **0为成功、1为失败** |
| message| String| 执行结果消息 |
| data | Object | 执行的结果集 |

- **返回示例**
>
```java
{
  "status": 0,
  "message": "成功！",
  "data": {
    "needUpdate" : true,  //是否需要更新
    "forceUpdate" : true,  //是否需要更新
    "fullPackageMd5" : "6d399095-349b-4a1f-b419-475bf829f03b", //全量包MD5，校验使用
    "fullPackageUrl" : "http://www.lovefe.fun/rnassets/full/app_1/native_5.8.0/rn_4_20181107__6d399095-349b-4a1f-b419-475bf829f03b.zip",
    "patchMd5" : "64b31ed9-bb1e-48c6-9d65-a2bce5d4b579", //增量包MD5
    "patchUrl" : "http://www.lovefe.fun/diff/app_1/native_5.8.0/rn_10/native_5.8.0_diff_10_4_20181108__64b31ed9-bb1e-48c6-9d65-a2bce5d4b579.zip",
    "updateRNVersion" : 10, //RN的版本
  }
}
```


### 接口说明 **2、** 下载成功统计接口

- **请求URL**
> [/rn/activeLog](#)

- **请求方式**
>**POST**

- **请求参数**
>
| 请求参数      |     参数类型 |   参数说明   |
| :-------- | :--------| :------ |
| appKey|  <mark>String,**必选**</mark>| 热更新APP的KEY  |
| clientVersion | <mark>String,**必选**</mark> | 客户端版本号 |
| fromVersion | <mark>Integer,**必选**</mark> | 上一个RN代码版本 |
| toVersion | <mark>Integer,**必选**</mark> | 下载成功的RN代码版本 |
| downloadType | <mark>String,**必选**</mark> | **patch:下载增量包,full:下载全量包** |


### 接口说明 **3、** RN加载激活成功统计接口

- **请求URL**
> [/rn/downloadLog](#)

- **请求方式**
>**POST**

- **请求参数**
>
| 请求参数      |     参数类型 |   参数说明   |
| :-------- | :--------| :------ |
| appKey|  <mark>String,**必选**</mark>| 热更新APP的KEY  |
| clientVersion | <mark>String,**必选**</mark> | 客户端版本号 |
| fromVersion | <mark>Integer,**必选**</mark> | 上一个RN代码版本 |
| toVersion | <mark>Integer,**必选**</mark> | 下载成功的RN代码版本 |


### 接口说明 **4、** RN版本回滚统计接口

- **请求URL**
> [/rn/rollbackLog](#)

- **请求方式**
>**POST**

- **请求参数**
>
| 请求参数      |     参数类型 |   参数说明   |
| :-------- | :--------| :------ |
| appKey|  <mark>String,**必选**</mark>| 热更新APP的KEY  |
| clientVersion | <mark>String,**必选**</mark> | 客户端版本号 |
| fromVersion | <mark>Integer,**必选**</mark> | 上一个RN代码版本 |
| toVersion | <mark>Integer,**必选**</mark> | 下载成功的RN代码版本 |
| msg | String | 发生错误导致版本回滚的原因 |
