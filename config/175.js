/**
 * Create by wangcheng on 11/1/18
 */

//热更新版本包记录的数据库，公用热更新后台数据库
const mysqlConfig = {
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'noah_system'
};

//node服务端口号
const serverConfig = {
    port : 3000
};

const packageConfig = {
    //设置RN全量、增量包根目录，和热更新后台系统设置的保持一致
    rootDir : '/home/admin/packages'
};

module.exports = {
    mysqlConfig,
    serverConfig,
    packageConfig
};