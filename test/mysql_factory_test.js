

const RNPackage = require('../model/RNPackage');

async function testGetAppByKey() {
    const appKey = 'e5ec40b0-325b-4908-9443-7b0403a1';
    const appVersion = 50709;
    const rnVersion = 5;
    const appResult = await RNPackage.getAppByKey(appKey);
    const fullPackage = await RNPackage.getFullPackage(appResult.id, appVersion, rnVersion);
    const patchPackage = await RNPackage.getPatchPackage(appResult.id, fullPackage.id, rnVersion);
    console.log(appResult);
    console.log(fullPackage);
    console.log(patchPackage);
}



testGetAppByKey();

