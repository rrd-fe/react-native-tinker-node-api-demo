
class Singleton {

    constructor(name){
        this.name = name;
        this.instance = null;
    }

    static getInstance(name){
        if(!this.instance){
            this.instance = new Singleton(name);
        }
        return this.instance;
    }

}


const instanceB = Singleton.getInstance('wangchengB');
console.log(instanceB.name);

const instanceA = Singleton.getInstance('wangcheng');
console.log(instanceA.name);

